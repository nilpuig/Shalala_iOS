

import UIKit
import Parse

import AudioToolbox




// MARK: - CUSTOM NICKNAME CELL
class ChatsCell: UITableViewCell {
    /* Views */
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var lastMessLabel: UILabel!
    @IBOutlet weak var adImage: UIImageView!
    @IBOutlet weak var senderLabel: UILabel!
}







// MARK: - CHATS CONTROLLER
class Chats: UIViewController,
UITableViewDataSource,
UITableViewDelegate,
UISearchBarDelegate

{

    /* Views */
    @IBOutlet weak var chatsTableView: UITableView!
    @IBOutlet weak var noChatsView: UIView!

    /* Variables */
    var chatsArray = [PFObject]()
    
    
    
override func viewDidAppear(_ animated: Bool) {
    queryChats()
}
    
override func viewDidLoad() {
        super.viewDidLoad()

}



    
// QUERY CHATS
func queryChats() {
    chatsArray.removeAll()
    showHUD("Please wait...")
    
    // Make query
    let query = PFQuery(className: CHATS_CLASS_NAME)
    query.includeKey(USER_CLASS_NAME)
    query.whereKey(CHATS_ID, contains: "\(PFUser.current()!.objectId!)")
    query.order(byDescending: "createdAt")
    
    query.findObjectsInBackground { (objects, error)-> Void in
        if error == nil {
            self.chatsArray = objects!
            self.hideHUD()
            
            if self.chatsArray.count == 0 {
                self.noChatsView.isHidden = false
            } else {
                self.noChatsView.isHidden = true
                self.chatsTableView.reloadData()
            }
            
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}

}
    
    
   
    
    
// MARK: - TABLEVIEW DELEGATES
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return chatsArray.count
}
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ChatsCell", for: indexPath) as! ChatsCell
    
    var chatsObj = PFObject(className: CHATS_CLASS_NAME)
    chatsObj = chatsArray[indexPath.row]
    
    // Get User Pointer
    let userPointer = chatsObj[CHATS_USER_POINTER] as! PFUser
    userPointer.fetchIfNeededInBackground { (user, error) in
    
        let otherUser = chatsObj[CHATS_OTHER_USER] as! PFUser
        otherUser.fetchIfNeededInBackground(block: { (user2, error) in
            if error == nil {
                
                // Get AdPointer
                let adPointer = chatsObj[CHATS_AD_POINTER] as! PFObject
                adPointer.fetchIfNeededInBackground { (user, error) in
                    
                    // Get Ad image
                    let imageFile = adPointer[RIDE_IMAGE1] as? PFFile
                    imageFile?.getDataInBackground(block: { (imageData, error) -> Void in
                        if error == nil {
                            if let imageData = imageData {
                                cell.adImage.image = UIImage(data:imageData)
                    }}})
                    
                    // Get Ad title
                    cell.usernameLabel.text = "\(adPointer[RIDE_TITLE]!)"
               
                }// end adPointer
                
                
                // Get Sender's username
                if userPointer.objectId == PFUser.current()!.objectId {
                    cell.senderLabel.text = "You wrote:"

                } else {
                    cell.senderLabel.text = "@\(userPointer[USER_USERNAME]!)"
                }
                
                
                // Get last Message
                cell.lastMessLabel.text = "\(chatsObj[CHATS_LAST_MESSAGE]!)"
                
                
                // Get Date
                let cDate = chatsObj.createdAt!
                let date = Date()
                cell.dateLabel.text = self.timeAgoSinceDate(cDate, currentDate: date, numericDates: true)
 
                
            // error in otherUser
            } else { self.simpleAlert("\(error!.localizedDescription)")
        }}) // end otherUser
    
        
    }// end userPointer
    
    
    
return cell
}
    
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
}
    
    
    
// MARK: -  CELL HAS BEEN TAPPED -> CHAT WITH THE SELECTED CHAT
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var chatsObj = PFObject(className: CHATS_CLASS_NAME)
    chatsObj = chatsArray[indexPath.row]
    
    // Get adPointer
    let adPointer = chatsObj[CHATS_AD_POINTER] as! PFObject
    
    // Get userPointer
    let userPointer = chatsObj[CHATS_USER_POINTER] as! PFUser
    userPointer.fetchIfNeededInBackground { (user, error) in
        
        let otherUser = chatsObj[CHATS_OTHER_USER] as! PFUser
        otherUser.fetchIfNeededInBackground(block: { (user2, error) in
            if error == nil {
                let currentUser = PFUser.current()!
                let blockedUsers = otherUser[USER_HAS_BLOCKED] as! [String]
                
                // otherUser user has blocked you
                if blockedUsers.contains(currentUser.objectId!) {
                    self.simpleAlert("Sorry, @\(otherUser[USER_USERNAME]!) has blocked you. You can't chat with this user.")
                
                // Chat with otherUser
                } else {
                    let inboxVC = self.storyboard?.instantiateViewController(withIdentifier: "Inbox") as! Inbox
        
                    if userPointer.objectId == PFUser.current()!.objectId {
                        inboxVC.userObj = otherUser
                    } else {
                        inboxVC.userObj = userPointer
                    }
                    
                    // Pass the adPointer
                    inboxVC.adObj = adPointer
                    
                    self.navigationController?.pushViewController(inboxVC, animated: true)
                }
                
            } else { self.simpleAlert("\(error!.localizedDescription)")
        }})

    }
    
}
    
    
    
    
    
    
   
    
    
// MARK: - BACK BUTTON
@IBAction func backButt(_ sender: Any) {
    _ = navigationController?.popViewController(animated: true)
}
    
    
    
    
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}




