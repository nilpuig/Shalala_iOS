
import Foundation
import UIKit
import AVFoundation
import CoreLocation




// IMPORTANT: Replace the red string below with the new name you'll give to this app
let APP_NAME = "Shalala"



// IMPORTANT: Reaplce the red strings below with your own Application ID and Client Key of your app on back4app.com
let PARSE_APP_ID = "5ydOppunSfhkDVRJgsact1qzazKLaFXl2yoZ4Wre"
let PARSE_CLIENT_KEY = "N3vErkpMi6DNFVZH70wRUudC4YPiAc3AcFuoQSMD"
//-----------------------------------------------------------------------------



// IMPORTANT: REPLACE THE RED STRING BELOW WITH YOUR OWN BANNER UNIT ID YOU'LL GET FROM  http://apps.admob.com
let ADMOB_BANNER_UNIT_ID = ""




// THIS IS THE RED MAIN COLOR OF THIS APP
//let MAIN_COLOR = UIColor(red: 250/255, green: 51/255, blue: 74/255, alpha: 1.0)
let MAIN_COLOR = UIColor(red:0.00, green:0.82, blue:0.73, alpha:1.0)


// THIS IS THE MAX DURATION OF A VIDEO RECORDING FOR AN AD (in seconds)
let MAXIMUM_DURATION_VIDEO:TimeInterval = 360



// REPLACE THE RED STRINGS BELOW WITH YOUR OWN TEXTS FOR THE EACH WIZARD'S PAGE
let wizardLabels = [
    "SELL QUICKLY, CHAT TO BUY\n\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.",
    
    "LIST YOUR ITEM IN NO TIME\n\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. ",
    
    "PRIVATELY TALK TO SELLERS\n\nLorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. ",
]



// YOU CAN CHANGE THE AD REPORT OPTIONS BELOW AS YOU WISH
let reportAdOptions = [
    "Violence",
    "Harassment",
    "Spam",
    "Nudity",
    "Unathorized Sales",
    "Hate speech",
    "No-show on meetup",
    "Backed out of deal"
]


// YOU CAN CHANGE THE USER REPORT OPTIONS BELOW AS YOU WISH
let reportUserOptions = [
    "Violence",
    "Harassment",
    "Spam",
    "Nudity",
    "Unathorized Sales",
    "Hate speech",
    "No-show on meetup",
    "Backed out of deal"
]



// HUD View extension
let hudViewWidth = 87.0
let hudView = UIView(frame: CGRect(x:0, y:0, width:hudViewWidth, height: hudViewWidth))
let label = UILabel()
let indicatorView = UIActivityIndicatorView(frame: CGRect(x:0, y:0, width:50, height:50))
extension UIViewController {
    func showHUD(_ mess:String) {
        hudView.center = CGPoint(x: view.frame.size.width/2, y: view.frame.size.height/2)
        
        // Hudview color
        //hudView.backgroundColor = MAIN_COLOR
        hudView.backgroundColor = UIColor(red:0.52, green:0.52, blue:0.52, alpha:0.5)
        hudView.alpha = 1.0
        hudView.layer.cornerRadius = 8
        
        indicatorView.center = CGPoint(x: hudView.frame.size.width/2, y: hudView.frame.size.height/2)
        indicatorView.activityIndicatorViewStyle = .white
        hudView.addSubview(indicatorView)
        indicatorView.startAnimating()
        view.addSubview(hudView)
        
        label.frame = CGRect(x: 0, y: hudViewWidth/1.3, width: hudViewWidth, height: 20)
        label.font = UIFont(name: "Titillium-Semibold", size: 13)
        label.text = mess
        label.textAlignment = .center
        label.textColor = UIColor.white
        hudView.addSubview(label)
    }
    
    func hideHUD() {
        hudView.removeFromSuperview()
        label.removeFromSuperview()
    }
    
    func simpleAlert(_ mess:String) {
        let alert = UIAlertController(title: APP_NAME, message: mess, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in })
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    
    
    // SHOW LOGIN ALERT
    func showLoginAlert(_ mess:String) {
        let alert = UIAlertController(title: APP_NAME,
            message: mess,
            preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Login", style: .default, handler: { (action) -> Void in
            let aVC = self.storyboard?.instantiateViewController(withIdentifier: "Wizard") as! Wizard
            self.present(aVC, animated: true, completion: nil)

        })
        
        // Cancel button
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })

        alert.addAction(ok)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    
    
    
    // MARK: - SCALE IMAGE PROPORTIONALLY
    func scaleImage(image: UIImage, maxDimension: CGFloat) -> UIImage {
        
        var scaledSize = CGSize(width: maxDimension, height: maxDimension)
        var scaleFactor: CGFloat
        
        if image.size.width > image.size.height {
            scaleFactor = image.size.height / image.size.width
            scaledSize.width = maxDimension
            scaledSize.height = scaledSize.width * scaleFactor
        } else {
            scaleFactor = image.size.width / image.size.height
            scaledSize.height = maxDimension
            scaledSize.width = scaledSize.height * scaleFactor
        }
        
        UIGraphicsBeginImageContext(scaledSize)
        image.draw(in: CGRect(x:0, y:0, width: scaledSize.width, height: scaledSize.height))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
}








/****** DO NOT EDIT THE CODE BELOW *****/
let USER_CLASS_NAME = "_User"
let USER_USERNAME = "username"
let USER_EMAIL = "email"
let USER_EMAIL_VERIFIED = "emailVerified"
let USER_FULLNAME = "fullName"
let USER_AVATAR = "avatar"
let USER_LOCATION = "location"
let USER_ABOUT_ME = "aboutMe"
let USER_WEBSITE = "website"
let USER_IS_REPORTED = "isReported"
let USER_REPORT_MESSAGE = "reportMessage"
let USER_HAS_BLOCKED = "hasBlocked"

let CATEGORIES_CLASS_NAME = "Categories"
let CATEGORIES_CATEGORY = "category"
let CATEGORIES_IMAGE = "image"

let RIDE_CLASS_NAME = "Ads"
let RIDE_SELLER_POINTER = "sellerPointer"
let RIDE_LIKED_BY = "likedBy" // Array
let RIDE_KEYWORDS = "keywords" // Array
let RIDE_TITLE = "title"
let RIDE_DATE = "date"
let RIDE_PRICE = "price"
let RIDE_SEAT_NUMBER = "seatNumber"
let RIDE_CURRENCY = "currency"
let RIDE_LEAVING_FROM = "leavingFrom"
let RIDE_GOING_TO = "goingTo"
let RIDE_LOCATION = "location"
let RIDE_IMAGE1 = "image1"
let RIDE_IMAGE2 = "image2"
let RIDE_IMAGE3 = "image3"
let RIDE_VIDEO = "video"
let RIDE_VIDEO_THUMBNAIL = "videoThumbnail"
let RIDE_DESCRIPTION = "description"
let RIDE_LIKES = "likes"
let RIDE_COMMENTS = "comments"
let RIDE_IS_REPORTED = "isReported"
let RIDE_REPORT_MESSAGE = "reportMessage"


let LIKES_CLASS_NAME = "Likes"
let LIKES_CURR_USER = "currUser"
let LIKES_AD_LIKED = "adLiked"

let COMMENTS_CLASS_NAME = "Comments"
let COMMENTS_USER_POINTER = "userPointer"
let COMMENTS_AD_POINTER = "adPointer"
let COMMENTS_COMMENT = "comment"

let ACTIVITY_CLASS_NAME = "Activity"
let ACTIVITY_CURRENT_USER = "currUser"
let ACTIVITY_OTHER_USER = "otherUser"
let ACTIVITY_TEXT = "text"


let INBOX_CLASS_NAME = "Inbox"
let INBOX_AD_POINTER = "adPointer"
let INBOX_SENDER = "sender"
let INBOX_RECEIVER = "receiver"
let INBOX_INBOX_ID = "inboxID"
let INBOX_MESSAGE = "message"
let INBOX_IMAGE = "image"

let CHATS_CLASS_NAME = "Chats"
let CHATS_LAST_MESSAGE = "lastMessage"
let CHATS_USER_POINTER = "userPointer"
let CHATS_OTHER_USER = "otherUser"
let CHATS_ID = "chatID"
let CHATS_AD_POINTER = "adPointer"

let FEEDBACKS_CLASS_NAME = "Feedbacks"
let FEEDBACKS_AD_TITLE = "adTitle"
let FEEDBACKS_SELLER_POINTER = "sellerPointer"
let FEEDBACKS_REVIEWER_POINTER = "reviewerPointer"
let FEEDBACKS_STARS = "stars"
let FEEDBACKS_TEXT = "text"




/* Global Variables */
var distanceInMiles:Double = 50
var sortBy = "Recent"
var selectedCategory = "All cities"
var selLeavingFrom = "All cities"
var selGoingTo = "All cities"
var selectedDate = "All dates"
var selectedDateSellEdit = ""
var chosenLocation:CLLocation?
var previousPage = "Home"
var goingTo = true



// MARK: - METHOD TO CREATE A THUMBNAIL OF YOUR VIDEO
func createVideoThumbnail(_ url:URL) -> UIImage? {
    let asset = AVAsset(url: url)
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    imageGenerator.appliesPreferredTrackTransform = true
    var time = asset.duration
    time.value = min(time.value, 2)
    do { let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
        return UIImage(cgImage: imageRef)
    } catch let error as NSError {
        print("Image generation failed with error \(error)")
        return nil
    }
}


// MARK: - EXTENSION TO RESIZE A UIIMAGE
extension UIViewController {
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}



// EXTENSION TO FORMAT LARGE NUMBERS INTO K OR M (like 1.1M, 2.5K)
extension Int {
    var abbreviated: String {
        let abbrev = "KMBTPE"
        return abbrev.characters.enumerated().reversed().reduce(nil as String?) { accum, tuple in
            let factor = Double(self) / pow(10, Double(tuple.0 + 1) * 3)
            let format = (factor.truncatingRemainder(dividingBy: 1)  == 0 ? "%.0f%@" : "%.1f%@")
            return accum ?? (factor > 1 ? String(format: format, factor, String(tuple.1)) : nil)
        } ?? String(self)
    }
}



// EXTENSION TO SHOW TIME AGO DATES
extension UIViewController {
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 h ago"
            } else {
                return "An h ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds"
        } else {
            return "Just now"
        }
        
    }
    
}





