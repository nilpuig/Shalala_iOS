

import UIKit
import Parse
import CoreLocation
import AudioToolbox


// MARK: - AD CELL
class AdCell: UICollectionViewCell {
    /* Views */
    @IBOutlet weak var adImage: UIImageView!
    @IBOutlet weak var adTitleLabel: UILabel!
    @IBOutlet weak var adPriceLabel: UILabel!
    @IBOutlet weak var adTimeLabel: UILabel!
    @IBOutlet weak var adSeatNumber: UILabel!
    
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var likeOutlet: UIButton!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentsOutlet: UIButton!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var optionOutlet: UIButton!
    @IBOutlet weak var avatarOutlet: UIButton!
    
}






// MARK: - ADS LIST CONTROLLER
class AdsList: UIViewController,
CLLocationManagerDelegate,
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout,
UITextFieldDelegate
{

    /* Views */

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var cancelOutlet: UIButton!
    
    @IBOutlet weak var adsCollView: UICollectionView!
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var sortLabel: UILabel!
    @IBOutlet weak var leavingFromLabel: UILabel!
    
    @IBOutlet weak var noResultsView: UIView!
    
    
    
    
    
    /* Variables */
    var searchTxt = ""
    var adsArray = [PFObject]()
    var cellSize = CGSize()
    
    
    
override func viewDidAppear(_ animated: Bool) {
    
    // Reset data
    adsArray.removeAll()
    adsCollView.reloadData()
    
    
    // Set search variables for the query
    if searchTxt != "" {
        searchTextField.text = searchTxt
    } else {
        searchTextField.text = selGoingTo
    }
    
    categoryLabel.text = selGoingTo
    leavingFromLabel.text = selLeavingFrom
    
    
    sortLabel.text = sortBy
    
     self.queryRides()
}

    
    
    
override func viewDidLoad() {
        super.viewDidLoad()

    // Layouts
    let placeholder = searchTextField.value(forKey: "placeholderLabel") as? UILabel
    placeholder?.textColor = UIColor.init(white: 255, alpha: 0.5)
    
    adsCollView.backgroundColor = UIColor.clear
    noResultsView.isHidden = true

    
    // Set cells size
    cellSize = CGSize(width: view.frame.size.width/1.163, height: 400)
    
    /*
    if UIDevice.current.userInterfaceIdiom == .pad {
        cellSize = CGSize(width: view.frame.size.width/3 - 20, height: 236)
    } else {
        cellSize = CGSize(width: view.frame.size.width/2 - 20, height: 236)
    } */

}

    
    
    
// MARK: - QUERY RIDES
func queryRides() {
    noResultsView.isHidden = true
    
    let keywords = searchTxt.lowercased().components(separatedBy: " ")
    showHUD("Please wait...")
    print("KEYWORDS: \(keywords)\nCATEGORY: \(selGoingTo)")
    
    let query = PFQuery(className: RIDE_CLASS_NAME)
    
    // query by text and/or Category
    if searchTxt != "" { query.whereKey(RIDE_KEYWORDS, containedIn: keywords) }
    if selLeavingFrom != "All cities" { query.whereKey(RIDE_LEAVING_FROM, equalTo: selLeavingFrom) }
    if selGoingTo != "All cities" { query.whereKey(RIDE_GOING_TO, equalTo: selGoingTo) }
    
    // query sortBy
    switch sortBy {
        case "Recent": query.order(byDescending: "createdAt")
        case "Lower Price": query.order(byAscending: RIDE_PRICE)
        case "Higher Price": query.order(byDescending: RIDE_PRICE)
        case "Higher number of seats": query.order(byDescending: RIDE_SEAT_NUMBER)
        case "Most Liked": query.order(byDescending: RIDE_LIKES)
        
    default:break}
    
    // Hide reported rides
    //query.whereKey(RIDE_IS_REPORTED, equalTo: false)
    
    // Query block
    query.findObjectsInBackground { (objects, error) in
        if error == nil {
            self.adsArray = objects!
            self.hideHUD()
            self.adsCollView.reloadData()
            
            // Show/hide noResult view
            if self.adsArray.count == 0 { self.noResultsView.isHidden = false
            } else { self.noResultsView.isHidden = true }
            
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
}
    


// MARK: - COLLECTION VIEW DELEGATES
func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
}
    
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return adsArray.count
}
    
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdCell", for: indexPath) as! AdCell

    // Get Ad Object
    var adObj = PFObject(className: RIDE_CLASS_NAME)
    adObj = adsArray[indexPath.row]
    
    // Get User Pointer
    let userPointer = adObj[RIDE_SELLER_POINTER] as! PFUser
    userPointer.fetchIfNeededInBackground(block: { (user, error) in
        if error == nil {
    
            // Get image 1
            let imageFile = adObj[RIDE_IMAGE1] as? PFFile
            imageFile?.getDataInBackground(block: { (data, error) in
                if error == nil { if let imageData = data {
                    cell.adImage.image = UIImage(data: imageData)
            }}})

            // Get title
            cell.adTitleLabel.text = "\(adObj[RIDE_TITLE]!)"
        
            // Get price
            cell.adPriceLabel.text = "\(adObj[RIDE_CURRENCY]!)\(adObj[RIDE_PRICE]!)"
            
            // Get number of seats
            cell.adSeatNumber.text = "\(adObj[RIDE_SEAT_NUMBER]!) available seats"

            // Get likes
            if adObj[RIDE_LIKES] != nil {
                let likes = adObj[RIDE_LIKES] as! Int
                cell.likesLabel.text =  likes.abbreviated
            } else { cell.likesLabel.text = "0" }
            
            // Get comments
            if adObj[RIDE_COMMENTS] != nil {
                let comments = adObj[RIDE_COMMENTS] as! Int
                cell.commentsLabel.text = comments.abbreviated
            } else { cell.commentsLabel.text = "0" }
            
            // Get date
            let currDate = Date()
            cell.adTimeLabel.text = self.timeAgoSinceDate(adObj.createdAt!, currentDate: currDate, numericDates: true)
            
            
            // Get User's avatar
            cell.avatarImg.layer.cornerRadius = cell.avatarImg.bounds.size.width/2
            let imageFile2 = userPointer[USER_AVATAR] as? PFFile
            imageFile2?.getDataInBackground(block: { (data, error) in
                if error == nil { if let imageData = data {
                    cell.avatarImg.image = UIImage(data: imageData)
            }}})
            
            // Get User's username
            cell.usernameLabel.text = "\(userPointer[USER_USERNAME]!)"
            
            
            
            //  CHECK IF YOU'VE ALREADY LIKED THIS AD AND CHAMGE LIKE ICON
            if PFUser.current() != nil {
                let currUserID = PFUser.current()!.objectId!
                if adObj[RIDE_LIKED_BY] != nil {
                    let likedByArr = adObj[RIDE_LIKED_BY] as! [String]
                    if likedByArr.contains(currUserID) {
                        cell.likeOutlet.setBackgroundImage(UIImage(named:"liked_icon"), for: .normal)
                    } else {
                        cell.likeOutlet.setBackgroundImage(UIImage(named:"like_icon"), for: .normal)
                    }
                } else {
                    cell.likeOutlet.setBackgroundImage(UIImage(named:"like_icon"), for: .normal)
                }
            }
            
            
            // cell layout
            cell.layer.cornerRadius = 6
            
            
            // Assign tags to buttons
            cell.likeOutlet.tag = indexPath.row
            cell.commentsOutlet.tag = indexPath.row
            cell.optionOutlet.tag = indexPath.row
            cell.avatarOutlet.tag = indexPath.row
            
            
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
    }})
    
return cell
}
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
}
    
    
    
// TAP ON A CELL -> SHOW AD's DETAILS
func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    // Get Ad Object
    var adObj = PFObject(className: RIDE_CLASS_NAME)
    adObj = adsArray[indexPath.row]
    
    let aVC = storyboard?.instantiateViewController(withIdentifier: "AdDetails") as! AdDetails
    aVC.adObj = adObj
    navigationController?.pushViewController(aVC, animated: true)
}
    
    
    
    @IBAction func dateBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Calendar", bundle: nil)
        let aVC = storyboard.instantiateInitialViewController()
        previousPage = "AdsList"
        self.navigationController?.pushViewController(aVC!, animated: false)
    }
    
    
    
// MARK: - SHOW CITIES
    @IBAction func leavingFromBtn(_ sender: Any) {
        goingTo = false
        
        let aVC = storyboard?.instantiateViewController(withIdentifier: "Categories") as! Categories
        present(aVC, animated: true, completion: nil)
    }
    @IBAction func categoryButt(_ sender: Any) {
        goingTo = true
        
        let aVC = storyboard?.instantiateViewController(withIdentifier: "Categories") as! Categories
        present(aVC, animated: true, completion: nil)
}
    
    
    
    
// MARK: - SORT BUTTON
@IBAction func sortButt(_ sender: Any) {
    let aVC = storyboard?.instantiateViewController(withIdentifier: "SortBy") as! SortBy
    present(aVC, animated: true, completion: nil)
}

    
    


    
// MARK: - AVATAR BUTTON
@IBAction func avatarButt(_ sender: UIButton) {
    let butt = sender
    
    var adObj = PFObject(className: RIDE_CLASS_NAME)
    adObj = adsArray[butt.tag]
    // Get User Pointer
    let userPointer = adObj[RIDE_SELLER_POINTER] as! PFUser
    userPointer.fetchIfNeededInBackground(block: { (user, error) in
        if error == nil {
            let aVC = self.storyboard?.instantiateViewController(withIdentifier: "UserProfile") as! UserProfile
            aVC.userObj = userPointer
            self.navigationController?.pushViewController(aVC, animated: true)
    }})
}
    
    
    
    
    
    
    
// MARK: - LIKE AD BUTTON
@IBAction func likeButt(_ sender: UIButton) {
    if PFUser.current() != nil {
    
        let indexP = IndexPath(row: sender.tag, section: 0)
        let cell = adsCollView.cellForItem(at: indexP) as! AdCell
    
    // Get Object
    var adObj = PFObject(className: RIDE_CLASS_NAME)
    adObj = adsArray[sender.tag]
    
    showHUD("Please wait...")
    let currUser = PFUser.current()!
    
    // 1. CHECK IF YOU'VE ALREADY LIKED THIS AD
    let query = PFQuery(className: LIKES_CLASS_NAME)
    query.whereKey(LIKES_CURR_USER, equalTo: currUser)
    query.whereKey(LIKES_AD_LIKED, equalTo: adObj)
    query.findObjectsInBackground { (objects, error) in
        if error == nil {
            
            
            // 2. LIKE THIS AD!
            if objects!.count == 0 {
                
                let likeObj = PFObject(className: LIKES_CLASS_NAME)
                
                // Save data
                likeObj[LIKES_CURR_USER] = currUser
                likeObj[LIKES_AD_LIKED] = adObj
                likeObj.saveInBackground(block: { (succ, error) in
                    if error == nil {
                        sender.setBackgroundImage(UIImage(named:"liked_icon"), for: .normal)
                        self.hideHUD()
                        
                        // Increment likes for the adObj
                        adObj.incrementKey(RIDE_LIKES, byAmount: 1)
                        
                        // Add the user's objectID
                        if adObj[RIDE_LIKED_BY] != nil {
                            var likedByArr = adObj[RIDE_LIKED_BY] as! [String]
                            likedByArr.append(currUser.objectId!)
                            adObj[RIDE_LIKED_BY] = likedByArr
                        } else {
                            var likedByArr = [String]()
                            likedByArr.append(currUser.objectId!)
                            adObj[RIDE_LIKED_BY] = likedByArr
                        }
                        adObj.saveInBackground()
                        
                        let likesNr = adObj[RIDE_LIKES] as! Int
                        cell.likesLabel.text = likesNr.abbreviated
                        
                        
                        // Send Push Notification
                        let sellerPointer = adObj[RIDE_SELLER_POINTER] as! PFUser
                        let pushStr = "@\(PFUser.current()![USER_USERNAME]!) liked your Ad: \(adObj[RIDE_TITLE]!)"
                        
                        let data = [ "badge" : "Increment",
                                     "alert" : pushStr,
                                     "sound" : "bingbong.aiff"
                        ]
                        let request = [
                            "someKey" : sellerPointer.objectId!,
                            "data" : data
                            ] as [String : Any]
                        PFCloud.callFunction(inBackground: "push", withParameters: request as [String : Any], block: { (results, error) in
                            if error == nil {
                                print ("\nPUSH SENT TO: \(sellerPointer[USER_USERNAME]!)\nMESSAGE: \(pushStr)\n")
                            } else {
                                print ("\(error!.localizedDescription)")
                            }
                        })
                        
                        
                        
                        // Save Activity
                        let activityClass = PFObject(className: ACTIVITY_CLASS_NAME)
                        activityClass[ACTIVITY_CURRENT_USER] = sellerPointer
                        activityClass[ACTIVITY_OTHER_USER] = PFUser.current()!
                        activityClass[ACTIVITY_TEXT] = pushStr
                        activityClass.saveInBackground()

                        
                    // error on saving like
                    } else {
                        self.simpleAlert("\(error!.localizedDescription)")
                        self.hideHUD()
                }})
                

                
                
            // 3. UNLIKE THIS AD :(
            } else {
                var likeObj = PFObject(className: LIKES_CLASS_NAME)
                likeObj = objects![0]
                likeObj.deleteInBackground(block: { (succ, error) in
                    if error == nil {
                        sender.setBackgroundImage(UIImage(named:"like_icon"), for: .normal)
                        self.hideHUD()
                        
                        // Decrement likes for the adObj
                        adObj.incrementKey(RIDE_LIKES, byAmount: -1)
                        
                        // Remove the user's objectID
                        var likedByArr = adObj[RIDE_LIKED_BY] as! [String]
                        likedByArr = likedByArr.filter { $0 != currUser.objectId! }
                        adObj[RIDE_LIKED_BY] = likedByArr
                        
                        adObj.saveInBackground()
                        
                        
                        let likesNr = adObj[RIDE_LIKES] as! Int
                        cell.likesLabel.text = likesNr.abbreviated
                        
                    } else {
                        self.simpleAlert("\(error!.localizedDescription)")
                        self.hideHUD()
                }})
            }
            
            
        // error in query
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
        
    } else {
        showLoginAlert("You need to be logged in to like this ad. Want to Login now?")
    }
}

    
    
    
    
    
    
    
// MARK: - COMMENTS BUTTON
@IBAction func commentsButt(_ sender: UIButton) {
    if PFUser.current() != nil {
        // Get Object
        var adObj = PFObject(className: RIDE_CLASS_NAME)
        adObj = adsArray[sender.tag]
    
        let aVC = storyboard?.instantiateViewController(withIdentifier: "Comments") as! Comments
        aVC.adObj = adObj
        navigationController?.pushViewController(aVC, animated: true)
    } else {
        showLoginAlert("You need to be logged in to comment this ad. Want to Login now?")
    }
}
    
    
    
    
    
    
// MARK: - AD's OPTION BUTTON
@IBAction func optionButt(_ sender: UIButton) {
    // Get Object and image1
    var adObj = PFObject(className: RIDE_CLASS_NAME)
    adObj = adsArray[sender.tag]
    
    var adImg = UIImage()
    let imageFile = adObj[RIDE_IMAGE1] as? PFFile
    imageFile?.getDataInBackground(block: { (data, error) in
        if error == nil { if let imageData = data {
            adImg = UIImage(data: imageData)!
    }}})

    
    let alert = UIAlertController(title: APP_NAME,
        message: "Select option",
        preferredStyle: .alert)
    
    
    // REPORT AD
    let report = UIAlertAction(title: "Report Ad", style: .default, handler: { (action) -> Void in
        let aVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportAdOrUser") as! ReportAdOrUser
        aVC.adObj = adObj
        aVC.reportType = "Ad"
        self.present(aVC, animated: true, completion: nil)
    })
    
    
    
    // SHARE AD
    let share = UIAlertAction(title: "Share", style: .default, handler: { (action) -> Void in
        
        let messageStr  = "Check this out: \(adObj[RIDE_TITLE]!) on #\(APP_NAME)"
        let img = adImg
        
        let shareItems = [messageStr, img] as [Any]
        
        let activityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            // iPad
            let popOver = UIPopoverController(contentViewController: activityViewController)
            popOver.present(from: .zero, in: self.view, permittedArrowDirections: .any, animated: true)
        } else {
            // iPhone
            self.present(activityViewController, animated: true, completion: nil)
        }
    })
    
    // Cancel button
    let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
    
    alert.addAction(report)
    alert.addAction(share)
    alert.addAction(cancel)
    present(alert, animated: true, completion: nil)
}
    
    
    
    
    
// MARK: - SEARCH TEXT FIELD DELEGATES
func textFieldDidBeginEditing(_ textField: UITextField) {
    cancelOutlet.isHidden = false
    textField.frame.size.width = view.frame.size.width - 124
    textField.frame.size.width = textField.frame.size.width - 58
}
    
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    searchTxt = textField.text!
    
    // Call query
    queryRides()
    
    textField.resignFirstResponder()
        
return true
}
    

// MARK: - CANCEL BUTTON
@IBAction func cancelButt(_ sender: Any) {
    searchTxt = ""
    searchTextField.text = ""
    searchTextField.resignFirstResponder()
    cancelOutlet.isHidden = true
    searchTextField.frame.size.width = view.frame.size.width - 124
    
    selGoingTo = "All cities"
    categoryLabel.text = selGoingTo
    queryRides()
}
    

    
    
    // MARK: - ACTIVITY BUTTON
    @IBAction func ActivityButt(_ sender: Any) {
        if PFUser.current() != nil {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "Activity") as! Activity
            navigationController?.pushViewController(aVC, animated: true)
        } else {
            showLoginAlert("You need to be logged in to see your Activity. Want to Login now?")
        }
    }
    
    
// MARK: - CHATS BUTTON
@IBAction func chatsButt(_ sender: Any) {
    if PFUser.current() != nil {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "Chats") as! Chats
        navigationController?.pushViewController(aVC, animated: true)
    } else {
        showLoginAlert("You need to be logged in to see your Chats. Want to Login now?")
    }
}
    
    
    
    
    
    
    
// MARK: - BACK BUTTON
@IBAction func backButt(_ sender: Any) {
    _ = navigationController?.popViewController(animated: true)
}
    
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
