

import UIKit
import Parse
import AudioToolbox
import CoreLocation
import MobileCoreServices
import AssetsLibrary
import MediaPlayer
import AVFoundation


extension String {
    
    var length: Int {
        return self.characters.count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
}

class Home: UIViewController,UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate{

    
    /* Views */
    @IBOutlet weak var leavingFrom: UIButton!
    @IBOutlet weak var goingTo: UIButton!
    @IBOutlet weak var calendar: UIButton!
    @IBOutlet weak var listTitle: UILabel!
    @IBOutlet weak var cityListView: UIView!
    @IBOutlet weak var citiesTableView: UITableView!
    
    /* Variables */
    var citiesArray = [PFObject]()
    var selectGoingTo = false
    var index : IndexPath? = nil
    
    
    override func viewDidAppear(_ animated: Bool) {
        // Associate the device with a user for Push Notifications
        if PFUser.current() != nil {
            let installation = PFInstallation.current()
            installation?["username"] = PFUser.current()!.username
            installation?["userID"] = PFUser.current()!.objectId!
            installation?.saveInBackground(block: { (succ, error) in
                if error == nil {
                    print("PUSH REGISTERED FOR: \(PFUser.current()!.username!)")
                }})
        }
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set date 
        if selectedDate != "All dates" {
            calendar.setTitle("\(selectedDate)"[0..<9], for: .normal)
        }
        else {
            calendar.setTitle("\(selectedDate)", for: .normal)
        }
        
        selLeavingFrom = "All cities"
        selGoingTo = "All cities"
        
        // Layouts
        //containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width,
                                               //  height: 800)
        
        cityListView.frame = CGRect(x: 0, y: 0,
                                      width: view.frame.size.width,
                                      height: view.frame.size.height)
        cityListView.frame.origin.y = view.frame.size.height
        
        self.citiesTableView.delegate = self;
        self.citiesTableView.dataSource = self;
        
        // Call silent query
        queryCities()
    }

    
    // MARK: - PICK UP/GOING TO BTN
    
    @IBAction func leavingFromBTN(_ sender: Any) {
        selectGoingTo = false
        showCities()
    }
    
    @IBAction func goingToBtn(_ sender: Any) {
        selectGoingTo = true
        showCities()
    }

    
    // MARK: - SHOW/HIDE CATEGORIES VIEW
    
    func showCities() {
        listTitle.text = "Choose a pick-up point"
        if (selectGoingTo) {
            listTitle.text = "Where are you going?"
        }
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
            self.cityListView.frame.origin.y = 0
        }, completion: { (finished: Bool) in })
    }
    
    func hideCities() {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
            self.cityListView.frame.origin.y = self.view.frame.size.height
        }, completion: { (finished: Bool) in })
    }
    
    
    @IBAction func doneBtn(_ sender: Any) {
        hideCities()
        
        // Deselect cell
        if let indexPath = index {
            citiesTableView.deselectRow(at: indexPath, animated:false)
        }
    }
    
    @IBAction func dateBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Calendar", bundle: nil)
        let aVC = storyboard.instantiateInitialViewController()
        previousPage = "Home"
        self.navigationController?.pushViewController(aVC!, animated: false)
    }
    
    // MARK: - FIND RIDE BTN
    @IBAction func findRideBtn(_ sender: Any) {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "AdsList") as! AdsList
        navigationController?.pushViewController(aVC, animated: true)
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - QUERY CITIES
    func queryCities() {
        showHUD("Please wait...")
        
        let query = PFQuery(className: CATEGORIES_CLASS_NAME)
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                self.citiesArray = objects!
                self.citiesTableView.reloadData()
                self.hideHUD()
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                self.hideHUD()
            }}
    }
    
    
    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return citiesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        var cObj = PFObject(className: CATEGORIES_CLASS_NAME)
        cObj = citiesArray[indexPath.row]
        cell.textLabel?.text = "\(cObj[CATEGORIES_CATEGORY]!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    // MARK: - CELL TAPPED -> SELECT CITY
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var cObj = PFObject(className: CATEGORIES_CLASS_NAME)
        cObj = citiesArray[indexPath.row]
        index = indexPath
        
        if (selectGoingTo) {
            goingTo.setTitle("\(cObj[CATEGORIES_CATEGORY]!)", for: .normal)
            selGoingTo = "\(cObj[CATEGORIES_CATEGORY]!)"
        }
        else {
            leavingFrom.setTitle("\(cObj[CATEGORIES_CATEGORY]!)", for: .normal)
            selLeavingFrom = "\(cObj[CATEGORIES_CATEGORY]!)"
        }
    }

}
