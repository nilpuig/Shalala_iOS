
import UIKit
import Parse
import AudioToolbox
import CoreLocation



// MARK: - CATEGORY CELL
class CategoryCell: UICollectionViewCell {
    /* Views */
    @IBOutlet weak var catLabel: UILabel!
    @IBOutlet weak var catImage: UIImageView!
}





// MARK: - HOME CONTROLLER
class HomeOld: UIViewController,
UITextFieldDelegate,
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout
{
    
    /* Views */
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var cancelOutlet: UIButton!
    
    @IBOutlet weak var categoriesCollView: UICollectionView!
    
    
    
    /* Variables */
    var categoriesArray = [PFObject]()
    var cellSize = CGSize()
    
    

    
    
override func viewDidAppear(_ animated: Bool) {
    // Associate the device with a user for Push Notifications
    if PFUser.current() != nil {
        let installation = PFInstallation.current()
        installation?["username"] = PFUser.current()!.username
        installation?["userID"] = PFUser.current()!.objectId!
        installation?.saveInBackground(block: { (succ, error) in
            if error == nil {
                print("PUSH REGISTERED FOR: \(PFUser.current()!.username!)")
        }})
    }
    
}
    
   

    
    
override func viewDidLoad() {
        super.viewDidLoad()

    // Layouts
    let placeholder = searchTextField.value(forKey: "placeholderLabel") as? UILabel
    placeholder?.textColor = UIColor.init(white: 255, alpha: 0.5)
    
    // Set cells size
    if UIDevice.current.userInterfaceIdiom == .pad {
        cellSize = CGSize(width: view.frame.size.width/3 - 20, height: view.frame.size.width/3 - 20)
    } else {
        cellSize = CGSize(width: view.frame.size.width/2 - 20, height: view.frame.size.width/2 - 20)
    }
    
    // Call query
    queryCategories()
}
    
 

// MARK: - QUERY CATEGORIRS
func queryCategories() {
    showHUD("Please wait...")
    
    let query = PFQuery(className: CATEGORIES_CLASS_NAME)
    query.findObjectsInBackground { (objects, error) in
        if error == nil {
            self.categoriesArray = objects!
            self.hideHUD()
            self.categoriesCollView.reloadData()
            
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
}
    
    
// MARK: - COLLECTION VIEW DELEGATES
func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
}
    
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return categoriesArray.count
}
    
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
    
    var cObj = PFObject(className: CATEGORIES_CLASS_NAME)
    cObj = categoriesArray[indexPath.row]
    
    cell.catLabel.text = "\(cObj[CATEGORIES_CATEGORY]!)".uppercased()
    
    let imageFile = cObj[CATEGORIES_IMAGE] as? PFFile
    imageFile?.getDataInBackground(block: { (data, error) in
        if error == nil { if let imageData = data {
            cell.catImage.image = UIImage(data: imageData)
    }}})

        
return cell
}
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
}

    
    
// TAP ON A CELL -> SHOW ADS
func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    var cObj = PFObject(className: CATEGORIES_CLASS_NAME)
    cObj = categoriesArray[indexPath.row]

    let aVC = storyboard?.instantiateViewController(withIdentifier: "AdsList") as! AdsList
    selectedCategory = "\(cObj[CATEGORIES_CATEGORY]!)"
    navigationController?.pushViewController(aVC, animated: true)
}
    

    
    
    
    
    
    
    
// MARK: - ENTER CHATS BUTTON
@IBAction func chatsButt(_ sender: Any) {
    if PFUser.current() != nil {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "Chats") as! Chats
        navigationController?.pushViewController(aVC, animated: true)
    } else {
        showLoginAlert("You need to be logged in to see your Chats. Want to Login now?")
    }
}
    
    

    
    
// MARK: - SEARCH TEXT FIELD DELEGATES
func textFieldDidBeginEditing(_ textField: UITextField) {
    cancelOutlet.isHidden = false
    textField.frame.size.width = view.frame.size.width - 84
    textField.frame.size.width = textField.frame.size.width - 48
}
    
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    // Go to AdsList
    let aVC = storyboard?.instantiateViewController(withIdentifier: "AdsList") as! AdsList
    aVC.searchTxt = textField.text!
    selectedCategory = "All cities"
    navigationController?.pushViewController(aVC, animated: true)
    
    textField.resignFirstResponder()

return true
}

@IBAction func cancelButt(_ sender: Any) {
    searchTextField.text = ""
    searchTextField.resignFirstResponder()
    cancelOutlet.isHidden = true
    searchTextField.frame.size.width = view.frame.size.width - 124
}
 
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
