//
//  LoadViewExampleViewController.swift
//  FSCalendarSwiftExample
//
//  Created by dingwenchao on 10/17/16.
//  Copyright © 2016 wenchao. All rights reserved.
//

import UIKit

class LoadViewExampleViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate {

    private weak var calendar: FSCalendar!
    
    let darkRed = UIColor(red:0.72, green:0.22, blue:0.00, alpha:1.0)
    
    override func loadView() {
        
        let view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor.groupTableViewBackground
        self.view = view
        
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        
        //let height: CGFloat = UIDevice.current.model.hasPrefix("iPad") ? 400 : 300
        let height: CGFloat = screenHeight/1.3
        let calYPos: CGFloat = (screenHeight - height)/3
        
        let calendar = FSCalendar(frame: CGRect(x: 0, y: calYPos, width: self.view.bounds.width, height: height))
        calendar.dataSource = self
        calendar.delegate = self
        calendar.backgroundColor = UIColor.white
        self.view.addSubview(calendar)
        self.calendar = calendar
        
        // Create 'I am Flexible' btn
        let widthBtn:CGFloat = 120
        let flexibleBtn = UIButton(frame: CGRect(x: screenSize.width/2-widthBtn/2, y: height+82, width: widthBtn, height: 40))
        flexibleBtn.setTitle("I am Flexible", for: .normal)
        flexibleBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        flexibleBtn.setTitleColor(darkRed, for: .normal)
        flexibleBtn.addTarget(self, action: #selector(selectAll), for: .touchUpInside)
        flexibleBtn.backgroundColor = .clear
        flexibleBtn.layer.cornerRadius = 20
        flexibleBtn.layer.borderWidth = 1
        flexibleBtn.layer.borderColor = darkRed.cgColor
        if (previousPage != "SellEdit") {
            self.view.addSubview(flexibleBtn)
        }
    }
    
    var dateString = ""
    
    @objc func selectAll(sender: UIButton!) {
        dateString = "All dates"
        goBack()
    }
    
    func goBack() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tbc = storyboard.instantiateViewController(withIdentifier: "MainTabBar") as! UITabBarController
        
        if (previousPage == "Home") {
            selectedDate = dateString
            tbc.selectedIndex = 0
        }
        else if (previousPage == "SellEdit") {
            selectedDateSellEdit = dateString
            tbc.selectedIndex = 1
        }
        else if (previousPage == "AdsList") {
            selectedDate = dateString
            tbc.selectedIndex = 0
        }
        self.present(tbc, animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Choose a date"
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        print ("Date: \(date)")
        dateString = "\(date)"
        goBack()
    }

}
